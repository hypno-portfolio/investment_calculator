package investment_calculator.service;

import investment_calculator.calculation.CalculateRepository;
import investment_calculator.calculation.Calculation;
import investment_calculator.investment.Investment;
import investment_calculator.investment.InvestmentRepository;
import investment_calculator.service.InvestmentService;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class InvestmentServiceTest {

    private Map<String, Investment> depositMap = new HashMap();
    private Map<String, Long> depositIdDepositNameMap = new HashMap<>();

    private Instant currentDate = null;
    private CalculateRepository mockCalculationRepository = new MockCalculationRepository();

    //    @Mock
    private InvestmentRepository mockInvestmentRepository = new MockInvestmentRepositoryInMemory();
    //    @InjectMocks
    private InvestmentService investmentService = new InvestmentService(mockInvestmentRepository, mockCalculationRepository);
    private CalculationResponse calculationResponse;
    private InvestmentCreatedResponse investmentCreatedResponse;
    private InvestmentToCreate depositParams;
    private List<InvestmentInfo> depositsInfo;
    private long nexDepositId = 0;
    private ArchivalCalculationResponse archivalCalculationResponse;


//    @BeforeEach
//    public void setUp() throws Exception{
//        MockitoAnnotations.initMocks(this);
//        when(mockInvestmentRepository.load("safetyAndSlow")).thenReturn(depositMap.get("depositMap"));
//    }

    @Test
    public void testCalculateIncomeImmediatelyOnePeriod() throws Exception {
        //given
        currentDateIs("11.01.2019");
        thereIsA(
                deposit("safetyAndSlow").
                        withInterest(10d).monthlyCapitalizationPeriod().
                        monthsSinceDepositStarted(12).andDays(0).
                        monthsToTerminateTheDeposit(12));
        //when
        calculateImmediately(
                IncomeForDeposit("safetyAndSlow").
                        withFounds(1000d));
        //then
        assertThatCalculationResponseHas(new Founds(1000d), new DateOfCalculation(now()), depositMap.get("safetyAndSlow"), CalculationAlgorithm.IMMEDIATELY, new Profit(2138.43));

    }


    @Test
    public void testCalculateIncomeImmediatelyOneAndAHalfPeriod() throws Exception {
        //given
        thereIsA(
                deposit("safetyAndSlow").
                        withInterest(10d).monthlyCapitalizationPeriod().
                        monthsSinceDepositStarted(1).andDays(15).
                        monthsToTerminateTheDeposit(12));
        //when
        calculateImmediately(
                IncomeForDeposit("safetyAndSlow").
                        withFounds(1000d));
        //then
        assertThatCalculationResponseHas(new Founds(1000d),
                new DateOfCalculation(now()), depositMap.get("safetyAndSlow"),
                CalculationAlgorithm.IMMEDIATELY, new Profit(155.00));
    }


    @Test
    public void testCalculateIncomeImmediately() throws Exception {
        //given
        currentDateIs("11.01.2019");
        thereIsA(
                deposit("safetyAndSlow").
                        withInterest(10d).monthlyCapitalizationPeriod().
                        monthsSinceDepositStarted(4).andDays(15).
                        monthsToTerminateTheDeposit(12));
        //when
        calculateImmediately(
                IncomeForDeposit("safetyAndSlow").
                        withFounds(1000d));
        //then
        assertThatCalculationResponseHas(new Founds(1000d), new DateOfCalculation(now()), depositMap.get("safetyAndSlow"), CalculationAlgorithm.IMMEDIATELY, new Profit(537.31));
    }

    @Test
    public void testCalculateIncomePeriodicallyOneAndAHalfPeriod() throws Exception {
        //given
        thereIsA(
                deposit("safetyAndSlow").
                        withInterest(10d).monthlyCapitalizationPeriod().
                        monthsSinceDepositStarted(1).andDays(15).
                        monthsToTerminateTheDeposit(12));
        //when
        calculatePeriodically(
                IncomeForDeposit("safetyAndSlow").
                        withFounds(1000d));
        //then
        assertThatCalculationResponseHas(new Founds(1000d), new DateOfCalculation(now()), depositMap.get("safetyAndSlow"), CalculationAlgorithm.PERIODICALLY, new Profit(100.00));
    }


    @Test
    public void testCreateAnExampleDeposit() throws Exception {
        //given
        paramsToCreateNewDeposit(depositName("testDeposit"), interest(10d), capitalizedEach("MONTH"), monthsToTerminateTheDeposit(12));

        //when
        createTheNewDeposit();
        //then
        assertThatThereIsCreatedDepositInDB();
        assertThatDepositCreatedResponseHas();
    }


    @Test
    public void testListAllDeposits() throws Exception {
        currentDateIs("11.01.2019");
        //given
        thereIsA(
                deposit("firstDeposit").
                        withInterest(10d).monthlyCapitalizationPeriod().
                        monthsSinceDepositStarted(1).andDays(15).
                        monthsToTerminateTheDeposit(12));
        thereIsA(
                deposit("secoundDeposit").
                        withInterest(10d).monthlyCapitalizationPeriod().
                        monthsSinceDepositStarted(12).andDays(0).
                        monthsToTerminateTheDeposit(12));
        //when
        listAllDeposits();

        //then
        assertThatThereAreOnTheListDepositInfoOf();
    }

    @Test
    public void testshowPreviouslyCalculationsForDeposit() throws Exception {
        //given
        thereIsA(
                deposit("safetyAndSlow").
                        withInterest(10d).monthlyCapitalizationPeriod().
                        monthsSinceDepositStarted(1).andDays(15).
                        monthsToTerminateTheDeposit(12));

        calculateImmediately(
                IncomeForDeposit("safetyAndSlow").
                        withFounds(1000d));

        calculatePeriodically(
                IncomeForDeposit("safetyAndSlow").
                        withFounds(1000d));
        //when
        askAboutArchivalCalculations("safetyAndSlow");

        //then
        assertThatInvestment("safetyAndSlow",
                hasArchivalClaculationResponse(
                        withCalculation().
                                founds(1000d).
                                dateOfCalculation().
                                calculationAlgorithm(CalculationAlgorithm.IMMEDIATELY).
                                profit(155.00),
                        andCalculation().
                                founds(1000d).
                                dateOfCalculation().
                                calculationAlgorithm(CalculationAlgorithm.PERIODICALLY).
                                profit(100.00)));
    }


    private void assertThatInvestment(String investmentName, List<Calculation> calculations) {
//            CalculationDescriptionAssembler assembler = new CalculationDescriptionAssembler();
        Investment investment = depositMap.get(investmentName);
        ArchivalCalculationResponse expectedArchivalCalculationResponse = new ArchivalCalculationResponse(
                depositIdDepositNameMap.get(investmentName),
                new InvestmentName(investmentName),
                investment.getInterest(),
                investment.getCapitalizationPeriod(),
                investment.getPeriodOfValidity().toDays(),
                calculations);

        assertThat(archivalCalculationResponse.equals(expectedArchivalCalculationResponse), is(true));
    }


    //Given--------------------------------------------------------------------------------------
    private DepositAssembler deposit(String depositeName) {
        return new DepositAssembler(depositeName);
    }

    private Instant date(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM.dd.uuuu HH:mm", Locale.UK);
        StringBuilder sb = new StringBuilder();
        sb.append(date).append(" 00:00");
        LocalDateTime localDateTime = LocalDateTime.parse(sb.toString(), formatter);
        ZoneId zoneId = ZoneId.of("GMT");
        ZonedDateTime zonedDateTime = localDateTime.atZone(zoneId);
        return zonedDateTime.toInstant();
    }

    private void currentDateIs(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM.dd.uuuu HH:mm", Locale.UK);
        StringBuilder sb = new StringBuilder();
        sb.append(date).append(" 00:00");
        LocalDateTime localDateTime = LocalDateTime.parse(sb.toString(), formatter);
        ZoneId zoneId = ZoneId.of("GMT");
        ZonedDateTime zonedDateTime = localDateTime.atZone(zoneId);
        currentDate = zonedDateTime.toInstant();
    }

    private Instant now() {
        return Instant.now();
    }

    private void thereIsA(DepositAssembler depositAssembler) {
        Investment investment = new Investment(depositAssembler.getDepositName(), depositAssembler.getInterest(), depositAssembler.getCapitalizationPeriod(), depositAssembler.getPeriodOfValidity());
        investment.setDepositId(nexDepositId);
        incrementNextDepositId();
        mockInvestmentRepository.save(investment);
        depositIdDepositNameMap.put(depositAssembler.getDepositName().getName(), 0l);
        depositMap.put(investment.getName(), investment);
    }


    private void paramsToCreateNewDeposit(String name, Double interest, CapitalizationPeriod period, PeriodOfValidity periodOfValidity) {
        InvestmentName investmentName = new InvestmentName(name);
        Interest depositInterest = new Interest(interest);
        depositMap.put("depositToCompare", new Investment(investmentName, depositInterest, period, periodOfValidity));
        this.depositParams = new InvestmentToCreate(name, interest, period, periodOfValidity);
    }

    private String depositName(String testDeposit) {
        return testDeposit;
    }

    private Double interest(double v) {
        return v;
    }

    private CapitalizationPeriod capitalizedEach(String period) {
        if (period.toUpperCase().equals("MONTH")) {
            return CapitalizationPeriod.MONTH;
        } else if (period.toUpperCase().equals("QUARTER")) {
            return CapitalizationPeriod.QUARTER;
        } else if (period.toUpperCase().equals("YEAR")) {
            return CapitalizationPeriod.YEAR;
        }
        return null;
    }


    private PeriodOfValidity monthsToTerminateTheDeposit(int months) {
        Instant timeOfDepositActivation = Instant.now().minusSeconds(2592000 * months);
        Instant timeOfDepositTerminated = Instant.now().plusSeconds(2592000 * months);
        PeriodOfValidity periodOfValidity = new PeriodOfValidity(timeOfDepositActivation, timeOfDepositTerminated);
        return periodOfValidity;
    }

    //When---------------------------------------------------------------------------------------
    private CalculationAssembler IncomeForDeposit(String depositName) {
        return new CalculationAssembler(depositName);
    }

    private void calculateImmediately(CalculationAssembler calculationAssembler) {
        calculateWithAlgorithm(calculationAssembler, CalculationAlgorithm.IMMEDIATELY);
    }

    private void calculatePeriodically(CalculationAssembler calculationAssembler) {
        calculateWithAlgorithm(calculationAssembler, CalculationAlgorithm.PERIODICALLY);
    }

    private void calculateWithAlgorithm(CalculationAssembler calculationAssembler, CalculationAlgorithm calculationAlgorithm) {
        this.calculationResponse =
                investmentService.
                        calculateIncome(calculationAssembler.gtId(), new Founds(calculationAssembler.getFounds()), calculationAlgorithm);
    }

    private void createTheNewDeposit() {
        this.investmentCreatedResponse = investmentService.createDeposit(depositParams);
        depositIdDepositNameMap.put(depositParams.getName().getName(), 0l);

    }

    private void listAllDeposits() {
        depositsInfo = investmentService.getDepositsInfo();
    }

    private void askAboutArchivalCalculations(String depositName) {
        archivalCalculationResponse = investmentService.getArchivalCalculations(depositIdDepositNameMap.get(depositName));
    }

    //Then---------------------------------------------------------------------------------------
    private void assertThatCalculationResponseHas(Founds founds, DateOfCalculation dateOfCalculation, Investment safetyAndSlow, CalculationAlgorithm immediately, Profit profit) {
        CalculationResponse calculationResponseToCompare = new CalculationResponse(founds, dateOfCalculation, safetyAndSlow, immediately, profit);
        assertThat(calculationResponse.equals(calculationResponseToCompare), is(true));
    }


    private void assertThatDepositCreatedResponseHas() {
        InvestmentCreatedResponse investmentCreatedResponse = new InvestmentCreatedResponse(0, depositParams.getName(), depositParams.getInterest(), depositParams.getPeriodOfValidity().toDays());
        assertThat(investmentCreatedResponse.equals(investmentCreatedResponse), is(true));
    }

    private void assertThatThereIsCreatedDepositInDB() {
        assertThat(mockInvestmentRepository.existsById(depositIdDepositNameMap.get("testDeposit")), is(true));
    }

    private void assertThatThereAreOnTheListDepositInfoOf() {
        InvestmentInfo investmentInfo1 = new InvestmentInfo(0l, new InvestmentName("firstDeposit"));
        InvestmentInfo investmentInfo2 = new InvestmentInfo(1l, new InvestmentName("secoundDeposit"));
        List<InvestmentInfo> depositToVerification = new LinkedList<>();
        depositToVerification.add(investmentInfo1);
        depositToVerification.add(investmentInfo2);

        assertThat(depositsInfo.get(0).equals(investmentInfo1), is(true));
        assertThat(depositsInfo.get(1).equals(investmentInfo2), is(true));
        assertThat(depositsInfo.equals(depositToVerification), is(true));

    }

    private List<Calculation> hasArchivalClaculationResponse(CalculationDescriptionAssembler... calculationDescriptionAssemblerParam) {
        List<Calculation> calculations = new LinkedList<>();

        for (CalculationDescriptionAssembler calculationDescriptionAssembler : calculationDescriptionAssemblerParam) {
            Calculation calculation = assemblerToCalculation(calculationDescriptionAssembler);
            calculations.add(calculation);
        }
        return calculations;
    }

    private Calculation assemblerToCalculation(CalculationDescriptionAssembler calculationDescriptionAssembler) {
        return new Calculation(calculationDescriptionAssembler.getFounds(), calculationDescriptionAssembler.getDateOfCalculation(), calculationDescriptionAssembler.getInvestment(), calculationDescriptionAssembler.getCalculationAlgorithm(), calculationDescriptionAssembler.getProfit());
    }

    private CalculationDescriptionAssembler withCalculation() {
        return new CalculationDescriptionAssembler();
    }

    private CalculationDescriptionAssembler andCalculation() {
        return new CalculationDescriptionAssembler();
    }

    private void incrementNextDepositId() {
        this.nexDepositId = nexDepositId + 1;
    }

    //Assembler----------------------------------------------------------------------------------
    private class CalculationAssembler {
        private final Investment investment;

        private Double founds;
        private Instant currentDate;

        private CalculationAssembler(String depositName) {
            investment = mockInvestmentRepository.findById(depositIdDepositNameMap.get(depositName)).orElse(null);
        }

        private CalculationAssembler withFounds(Double founds) {
            this.founds = founds;
            return this;
        }

        private CalculationAssembler at(Instant date) {
            this.currentDate = date;
            return this;
        }

        private Investment getInvestment() {
            return investment;
        }

        private Double getFounds() {
            return founds;
        }

        private Instant getCurrentDate() {
            return currentDate;
        }

        public Long gtId() {
            return 0l;
        }

    }

    private class DepositAssembler {
        Double interest = null;
        CapitalizationPeriod capitalizationPeriod = null;
        PeriodOfValidity periodOfValidity = null;
        private String depositName;

        private Instant timeOfDepositActivation = null;

        private DepositAssembler(String depositName) {
            this.depositName = depositName;
        }

        private InvestmentName getDepositName() {
            return new InvestmentName(depositName);
        }

        private Interest getInterest() {
            return new Interest(interest);
        }

        private CapitalizationPeriod getCapitalizationPeriod() {
            return capitalizationPeriod;
        }

        private PeriodOfValidity getPeriodOfValidity() {
            return periodOfValidity;
        }

        private DepositAssembler withInterest(Double interest) {
            this.interest = interest;
            return this;
        }

        private DepositAssembler monthlyCapitalizationPeriod() {
            this.capitalizationPeriod = CapitalizationPeriod.MONTH;
            return this;
        }

        private DepositAssembler andPeriodOfValidityIs(String startDate, String stopDate) {
            this.periodOfValidity = new PeriodOfValidity(date(startDate), date(stopDate));
            return this;
        }

        private DepositAssembler monthsSinceDepositStarted(int months) {
            this.timeOfDepositActivation = Instant.now().minusSeconds(2592000 * months);
            return this;
        }

        public DepositAssembler andDays(int days) {
            this.timeOfDepositActivation = timeOfDepositActivation.minusSeconds(86400 * days);
            return this;
        }

        private DepositAssembler monthsToTerminateTheDeposit(int months) {
            Instant timeOfDepositTerminated = Instant.now().plusSeconds(2592000 * months);
            this.periodOfValidity = new PeriodOfValidity(timeOfDepositActivation, timeOfDepositTerminated);
            return this;
        }
    }

    private class CalculationDescriptionAssembler {

        private Investment investment;
        private Founds founds;
        private DateOfCalculation dateOfCalculation;
        private CalculationAlgorithm calculationAlgorithm;
        private Profit profit;

        public CalculationDescriptionAssembler() {

        }

        public Investment getInvestment() {
            return investment;
        }

        public Founds getFounds() {
            return founds;
        }

        public DateOfCalculation getDateOfCalculation() {
            return dateOfCalculation;
        }

        public CalculationAlgorithm getCalculationAlgorithm() {
            return calculationAlgorithm;
        }

        public Profit getProfit() {
            return profit;
        }

        private CalculationDescriptionAssembler hasArchivalClaculationResponseWithCalculation(Founds founds) {
            this.founds = founds;
            return this;
        }

        private CalculationDescriptionAssembler dateOfCalculation(DateOfCalculation dateOfCalculation) {
            this.dateOfCalculation = dateOfCalculation;
            return this;
        }

        private CalculationDescriptionAssembler calculationAlgorithm(CalculationAlgorithm calculationAlgorithm) {
            this.calculationAlgorithm = calculationAlgorithm;
            return this;
        }

        private CalculationDescriptionAssembler profit(Double profit) {
            this.profit = new Profit(profit);
            return this;
        }

        private CalculationDescriptionAssembler dateOfCalculation() {
            this.dateOfCalculation = new DateOfCalculation(now());
            return this;
        }

        private CalculationDescriptionAssembler founds(double founds) {
            this.founds = new Founds(founds);
            return this;
        }
    }
}

