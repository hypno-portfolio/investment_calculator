package investment_calculator.web;

import investment_calculator.service.InvestmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/investments")
public class InvestmentController {
    private final InvestmentService investmentService;

    @Autowired
    public InvestmentController(InvestmentService investmentService) {
        this.investmentService = investmentService;
    }

    @GetMapping(produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public List<InvestmentInfo> getDeposits() {
        return investmentService.getDepositsInfo();
    }

    @PostMapping(produces = "application/json", consumes = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public InvestmentCreatedResponse createDeposit(@RequestBody InvestmentToCreate deposit) {
//    @ResponseStatus(HttpStatus.NO_CONTENT)
        return investmentService.createDeposit(deposit);
    }

    @PostMapping(value = "{depositId}/calculations", produces = "application/json", consumes = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public CalculationResponse calculateAQuotaOfAnIncome(@PathVariable Long depositId, @RequestBody CalculationRequest calculationRequest) {
        return investmentService.calculateIncome(depositId, calculationRequest.getFounds(), calculationRequest.getCalculationAlgorithm());
    }


    @GetMapping(value = "{depositId}/calculations", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public ArchivalCalculationResponse getArchivalCalculations(@PathVariable Long depositId) {
        return investmentService.getArchivalCalculations(depositId);
    }
}
