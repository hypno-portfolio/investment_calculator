package investment_calculator.web.request;

import investment_calculator.investment.ov.CapitalizationPeriod;
import investment_calculator.investment.ov.Interest;
import investment_calculator.investment.ov.InvestmentName;
import investment_calculator.investment.ov.PeriodOfValidity;

public class InvestmentToCreate {
    private String name;
    private double interest;
    private CapitalizationPeriod capitalizationPeriod;
    private PeriodOfValidity periodOfValidity;


    public InvestmentToCreate(String name, double interest, CapitalizationPeriod capitalizationPeriod, PeriodOfValidity periodOfValidity) {
        this.name = name;
        this.interest = interest;
        this.capitalizationPeriod = capitalizationPeriod;
        this.periodOfValidity = periodOfValidity;
    }

    public InvestmentToCreate() {
    }

    public InvestmentName getName() {
        return new InvestmentName(name);
    }

    public Interest getInterest() {
        return new Interest(interest);
    }

    public CapitalizationPeriod getCapitalizationPeriod() {
        return capitalizationPeriod;
    }

    public PeriodOfValidity getPeriodOfValidity() {
        return periodOfValidity;
    }
}
