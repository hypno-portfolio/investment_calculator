package investment_calculator.web.request;


import investment_calculator.calculation.ov.CalculationAlgorithm;
import investment_calculator.calculation.ov.Founds;

public class CalculationRequest {
    private double founds;
    private CalculationAlgorithm calculationAlgorithm;

    public CalculationRequest(double founds, CalculationAlgorithm calculationAlgorithm) {
        this.founds = founds;
        this.calculationAlgorithm = calculationAlgorithm;
    }

    public CalculationRequest() {
    }

    public Founds getFounds() {
        return new Founds(founds);
    }

    public CalculationAlgorithm getCalculationAlgorithm() {
        return calculationAlgorithm;
    }
}
