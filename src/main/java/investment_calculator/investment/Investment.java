package investment_calculator.investment;

import investment_calculator.investment.ov.CapitalizationPeriod;
import investment_calculator.investment.ov.Interest;
import investment_calculator.investment.ov.InvestmentName;
import investment_calculator.investment.ov.PeriodOfValidity;

import javax.persistence.*;
import java.util.List;

@Entity
public class Investment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "investment_id", updatable = false, nullable = false)
    private long depositId;
    @Embedded
    private InvestmentName name;
    @Column
    @Embedded
    private Interest interest;
    @Enumerated(value = EnumType.STRING)
    @Column
    private CapitalizationPeriod capitalizationPeriod;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "PeriodOfValidity_id")
    private PeriodOfValidity periodOfValidity = new PeriodOfValidity();

    public Investment(InvestmentName name, Interest interest, CapitalizationPeriod capitalizationPeriod, PeriodOfValidity periodOfValidity) {
        this.name = name;
        this.interest = interest ;
        this.capitalizationPeriod = capitalizationPeriod;
        this.periodOfValidity = periodOfValidity;
    }

    public Investment() {
    }

    public static Investment from(InvestmentToCreate investmentToCreate) {
        return new Investment(investmentToCreate.getName(), investmentToCreate.getInterest(), investmentToCreate.getCapitalizationPeriod(), investmentToCreate.getPeriodOfValidity());
    }


    public Calculation calculate(DateOfCalculation currentDate, Founds startingFounds, CalculationAlgorithm calculationAlgorithm) {
        Calculation calculation;
        if (periodOfValidity.isValid(currentDate)) {

            if (calculationAlgorithm.equals(CalculationAlgorithm.IMMEDIATELY)) {
                long daysSinceStarted = periodOfValidity.daysSinceStartedTo(currentDate);
                Profit newQuantum = accrueIncome(startingFounds, daysSinceStarted);
                calculation = new Calculation(startingFounds, currentDate, this, calculationAlgorithm, newQuantum);
                return calculation;
            } else if (calculationAlgorithm.equals(CalculationAlgorithm.PERIODICALLY)) {
                long daysInWholePeriondsOnly = periodOfValidity.daysInPeriondsSinceStartedTo(currentDate, capitalizationPeriod);
                Profit newQuantum = accrueIncome(startingFounds, daysInWholePeriondsOnly);
                calculation = new Calculation(startingFounds, currentDate, this, calculationAlgorithm, newQuantum);
                return calculation;
            }
        }
        return null;
    }

    public void setDepositId(long depositId) {
        this.depositId = depositId;
    }

    private Profit accrueIncome(Founds founds, double daysSinceStarted) {
        double period = capitalizationPeriod.daysInPeriod();
        double val = founds.getFounds();
        Double n = 1d;

        for (Double x = (daysSinceStarted / period); x > 0; x--) {
            if (x < 1) {
                n = Math.round(x * 10000.0) / 10000.0;
            }
            val = val + (val * (interest.getInterest()/100+1) - val) * n;
        }
        return new Profit(Math.round((val - founds.getFounds()) * 100.0) / 100.0);

    }

    public String getName() {
        return name.getName();
    }

    public InvestmentCreatedResponse toResponse() {
        return new InvestmentCreatedResponse(depositId, name, interest, periodOfValidity.toDays());
    }

    public ArchivalCalculationResponse toArchivalCalculationResponse(List<Calculation> calculations) {
        return new ArchivalCalculationResponse(depositId, name, interest, capitalizationPeriod, periodOfValidity.toDays(), calculations);
    }

    public Interest getInterest() {
        return interest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Investment)) return false;

        Investment investment = (Investment) o;

        if (interest != null ? !interest.equals(investment.interest) : investment.interest != null) return false;
        if (name != null ? !name.equals(investment.name) : investment.name != null) return false;
        if (capitalizationPeriod != investment.capitalizationPeriod) return false;
        return periodOfValidity != null ? periodOfValidity.equals(investment.periodOfValidity) : investment.periodOfValidity == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (depositId ^ (depositId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (interest != null ? interest.hashCode() : 0);
        result = 31 * result + (capitalizationPeriod != null ? capitalizationPeriod.hashCode() : 0);
        result = 31 * result + (periodOfValidity != null ? periodOfValidity.hashCode() : 0);
        return result;
    }

    public InvestmentInfo toInfo() {
        return new InvestmentInfo(depositId, name);
    }

    public CapitalizationPeriod getCapitalizationPeriod() {
        return capitalizationPeriod;
    }

    public PeriodOfValidity getPeriodOfValidity() {
        return periodOfValidity;
    }
}
