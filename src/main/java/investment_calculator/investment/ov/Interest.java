package investment_calculator.investment.ov;

import javax.persistence.Embeddable;

@Embeddable
public class Interest {
    private double interest;

    public Interest() {
    }

    public Interest(double interest) {
        this.interest = interest;
    }

    public double getInterest() {
        return interest;
    }

    public void setInterest(double interest) {
        this.interest = interest;
    }

}
