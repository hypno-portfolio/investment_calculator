package investment_calculator.investment.ov;

import javax.persistence.Embeddable;

@Embeddable
public class InvestmentName {
    private String name;

    public InvestmentName() {
    }

    public InvestmentName(String name) {
        this.name=name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
