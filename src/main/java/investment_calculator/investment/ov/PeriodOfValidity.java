package investment_calculator.investment.ov;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;

@Entity
public class PeriodOfValidity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "period_of_validity_id", updatable = false, nullable = false)
    long periodOfValidityId;
    @Column(name = "startdate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate startDate;
    @Column(name = "stopdate")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate stopDate;

    public PeriodOfValidity(Instant startDate, Instant stopDate) {
        Calendar calendar = Calendar.getInstance();
        this.startDate = getOnlyADate(startDate);
        this.stopDate = getOnlyADate(stopDate);
    }

    public PeriodOfValidity() {
    }

    private LocalDate getOnlyADate(Instant time) {
        return time.truncatedTo(ChronoUnit.DAYS).atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public boolean isValid(DateOfCalculation thatDate) {
        return thatDate.isLaterThat(stopDate);
    }

    public Long daysSinceStartedTo(DateOfCalculation date) {
        return date.daysSinceStarted(startDate);
    }

    public long daysInPeriondsSinceStartedTo(DateOfCalculation currentDate, CapitalizationPeriod capitalizationPeriod) {
        Long daysSinceStartedTo = daysSinceStartedTo(currentDate);
        Long daysInPeriondsSinceStartedTo = daysSinceStartedTo / capitalizationPeriod.daysInPeriod() * capitalizationPeriod.daysInPeriod();
        return daysInPeriondsSinceStartedTo;
    }

    public Integer toDays() {
        DateOfCalculation date = new DateOfCalculation(Instant.now());
        return daysSinceStartedTo(date).intValue();
    }
}
