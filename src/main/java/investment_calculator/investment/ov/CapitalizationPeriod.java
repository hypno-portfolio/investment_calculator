package investment_calculator.investment.ov;

public enum CapitalizationPeriod {
    MONTH("month", 30),
    QUARTER("quarter", 90),
    YEAR("year", 360);

    private final String periodName;
    private final long days;

    private CapitalizationPeriod(String periodName, long days) {
        this.periodName = periodName;
        this.days = days;
    }

    public String periodName() {
        return periodName;
    }

    public long daysInPeriod() {
        return days;
    }

}