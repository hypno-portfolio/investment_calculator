package investment_calculator.service;

import investment_calculator.calculation.CalculateRepository;
import investment_calculator.calculation.Calculation;
import investment_calculator.investment.Investment;
import investment_calculator.investment.InvestmentRepository;
import investment_calculator.web.request.InvestmentToCreate;
import investment_calculator.web.response.InvestmentCreatedResponse;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.LinkedList;
import java.util.List;


@Service
public class InvestmentService {
    private final InvestmentRepository investmentRepository;
    private final CalculateRepository calculateRepository;

    public InvestmentService(InvestmentRepository investmentRepository, CalculateRepository calculateRepository) {
        this.investmentRepository = investmentRepository;
        this.calculateRepository = calculateRepository;
    }

    public InvestmentCreatedResponse createDeposit(InvestmentToCreate investmentToCreate) {
        Investment investment = Investment.from(investmentToCreate);
        investmentRepository.save(investment);
        return investment.toResponse();
    }

    public List<InvestmentInfo> getDepositsInfo() {
        Iterable<Investment> deposits = investmentRepository.findAll();
        List<InvestmentInfo> depositsInfo = new LinkedList<>();
        for (Investment depost : deposits) {
            depositsInfo.add(depost.toInfo());
        }
        return depositsInfo;
    }

    public CalculationResponse calculateIncome(Long depositId, Founds founds, CalculationAlgorithm calculationAlgorithm) {
        Investment investment = findDeposit(depositId);
        DateOfCalculation currentDate = new DateOfCalculation(Instant.now());
        Calculation calculation = investment.calculate(currentDate, founds, calculationAlgorithm);
        calculateRepository.save(calculation);
        return calculation.toCalculationResponse();
    }

    public ArchivalCalculationResponse getArchivalCalculations(Long depositId) {
        List<Calculation> calculations = findAllArchivalCalculations(depositId);
        Investment investment = findDeposit(depositId);
        return investment.toArchivalCalculationResponse(calculations);
    }

    private Investment findDeposit(Long depositId) {
        Investment investment = investmentRepository.findById(depositId).orElseThrow(() -> new RuntimeException());
        return investment;
    }

    private List<Calculation> findAllArchivalCalculations(Long depositId) {
        Investment investment = findDeposit(depositId);
        List<Calculation> calculations = calculateRepository.findAllByInvestment(investment);
        if (calculations == null) {
            //throw an exception ?
        }
        return calculations;
    }
}
