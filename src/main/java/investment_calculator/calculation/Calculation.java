package investment_calculator.calculation;

import investment_calculator.calculation.ov.CalculationAlgorithm;
import investment_calculator.calculation.ov.DateOfCalculation;
import investment_calculator.calculation.ov.Founds;
import investment_calculator.calculation.ov.Profit;
import investment_calculator.investment.Investment;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

@Entity
public class Calculation {
    @Id
    @Column(name = "calculation_id", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long calculationId;
    @Column
    @Embedded
    private Founds founds;
    @Column
    @Embedded
    private DateOfCalculation dateOfCalculation;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "investment_id")
    private Investment investment;
    @Enumerated(EnumType.STRING)
    @Column
    private CalculationAlgorithm calculationAlgorithm;
    @Column
    @Embedded
    private Profit profit;

    public Calculation(Founds founds, DateOfCalculation dateOfCalculation, Investment investment, CalculationAlgorithm calculationAlgorithm, Profit profit) {
        this.founds = founds;
        this.dateOfCalculation = dateOfCalculation.getOnlyADate();
        this.investment = investment;
        this.calculationAlgorithm = calculationAlgorithm;
        this.profit = profit;
    }

    public Calculation() {
    }

    private Instant getOnlyADate(Instant time) {
        LocalDateTime localDateTime = time.truncatedTo(ChronoUnit.DAYS).atZone(ZoneId.systemDefault()).toLocalDate().atStartOfDay();
        ZoneId zoneId = ZoneId.of("GMT");
        ZonedDateTime zonedDateTime = localDateTime.atZone(zoneId);
        return zonedDateTime.toInstant();
    }


    public CalculationDescription getCalculationDescription() {
        return new CalculationDescription(founds, dateOfCalculation, calculationAlgorithm, profit);

    }

    public Profit getProfit() {
        return profit;
    }

    public long getCalculationId() {
        return calculationId;
    }

    public Founds getFounds() {
        return founds;
    }

    public DateOfCalculation getDateOfCalculation() {
        return dateOfCalculation;
    }

    public Investment getInvestment() {
        return investment;
    }

    public CalculationAlgorithm getCalculationAlgorithm() {
        return calculationAlgorithm;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Calculation that = (Calculation) o;

        if (founds != null ? !founds.equals(that.founds) : that.founds != null) return false;
        if (dateOfCalculation != null ? !dateOfCalculation.equals(that.dateOfCalculation) : that.dateOfCalculation != null)
            return false;
        if (investment != null ? !investment.equals(that.investment) : that.investment != null) return false;
        if (calculationAlgorithm != that.calculationAlgorithm) return false;
        return profit != null ? profit.equals(that.profit) : that.profit == null;
    }

    @Override
    public int hashCode() {
        int result = founds != null ? founds.hashCode() : 0;
        result = 31 * result + (dateOfCalculation != null ? dateOfCalculation.hashCode() : 0);
        result = 31 * result + (investment != null ? investment.hashCode() : 0);
        result = 31 * result + (calculationAlgorithm != null ? calculationAlgorithm.hashCode() : 0);
        result = 31 * result + (profit != null ? profit.hashCode() : 0);
        return result;
    }

    public CalculationResponse toCalculationResponse() {
        return new CalculationResponse(founds, dateOfCalculation, investment, calculationAlgorithm, profit);
    }
}
