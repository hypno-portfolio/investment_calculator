package investment_calculator.calculation;


import investment_calculator.investment.Investment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CalculateRepository extends CrudRepository<Calculation,Long>
{

    List<Calculation> findAllByInvestment(Investment investment);
}
