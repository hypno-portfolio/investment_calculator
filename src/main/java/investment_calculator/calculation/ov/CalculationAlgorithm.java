package investment_calculator.calculation.ov;

public enum CalculationAlgorithm {
    IMMEDIATELY,
    PERIODICALLY;
}
