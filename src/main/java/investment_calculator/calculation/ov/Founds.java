package investment_calculator.calculation.ov;


import javax.persistence.Embeddable;

@Embeddable
public class Founds {
    private double founds;

    public Founds() {
    }

    public Founds(double founds) {
        this.founds = founds;
    }

    public double getFounds() {
        return founds;
    }

    public void setFounds(double founds) {
        this.founds = founds;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Founds)) return false;

        Founds founds1 = (Founds) o;

        return Double.compare(founds1.founds, founds) == 0;
    }

    @Override
    public int hashCode() {
        long temp = Double.doubleToLongBits(founds);
        return (int) (temp ^ (temp >>> 32));
    }
}
