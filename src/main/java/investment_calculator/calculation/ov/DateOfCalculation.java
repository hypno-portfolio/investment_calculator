package investment_calculator.calculation.ov;


import javax.persistence.Embeddable;
import java.time.*;
import java.time.temporal.ChronoUnit;

@Embeddable
public class DateOfCalculation {
    private Instant dateOfCalculation;

    public DateOfCalculation() {
    }

    public DateOfCalculation(Instant dateOfCalculation) {
        this.dateOfCalculation = dateOfCalculation;
    }

    public Instant getDateOfCalculation() {
        return dateOfCalculation;
    }

    public void setDateOfCalculation(Instant dateOfCalculation) {
        this.dateOfCalculation = dateOfCalculation;
    }

    public Boolean isLaterThat(LocalDate stopDate) {
        if (dateOfCalculation.compareTo(getDate(stopDate)) > 0) {
            return false;
        }
        return true;
    }
    private Instant getDate(LocalDate date) {
        LocalDateTime localDateTime = date.atStartOfDay();
        ZoneId zoneId = ZoneId.of("GMT");
        ZonedDateTime zonedDateTime = localDateTime.atZone(zoneId);
        return zonedDateTime.toInstant();
    }

    public DateOfCalculation getOnlyADate() {
        LocalDateTime localDateTime = dateOfCalculation.truncatedTo(ChronoUnit.DAYS).atZone(ZoneId.systemDefault()).toLocalDate().atStartOfDay();
        ZoneId zoneId = ZoneId.of("GMT");
        ZonedDateTime zonedDateTime = localDateTime.atZone(zoneId);
        return new DateOfCalculation(zonedDateTime.toInstant());
    }


    public Long daysSinceStarted(LocalDate date) {
        int compareResult = getDate(date).compareTo(dateOfCalculation);
        if (compareResult > 0) {
            //exception?
        } else if (compareResult == 0) {
            return 0l;
        }
        return dateOfCalculation.minusSeconds(getDate(date).getEpochSecond()).getEpochSecond() / 86400;
    }


}