package investment_calculator.calculation.ov;

import javax.persistence.Embeddable;

@Embeddable
public class Profit {
    private double profit;

    public Profit() {
    }

    public Profit(double profit) {
        this.profit = profit;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }

}
